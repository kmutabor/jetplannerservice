<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 14.08.17
 * Time: 21:09
 */

class Auth
{
    /**
     * @var \app\Resource $resource
     */
    private $resource;


    public function __construct()
    {
        $this->resource = \app\Resource::getInstance();
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface $response PSR7 response
     * @param  callable $next Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
//        $requestData = $request->getQueryParams();
//        return $response->withJson([$requestData], 500);

        if ($request->isPost()) {
            $requestData = $request->getParsedBody();
        } else if ($request->isGet() || $request->isPut()) {
            $requestData = $request->getQueryParams();
        }else if ($request->isDelete()) {
            $data = $request->getAttributes()['routeInfo'][2];
            $requestData['user_id'] = $data['user_id'];
            $requestData['token'] = $data['token'];
        } else {
            return $response->withJson(['error' => 'request error', 'message' => 'invalid request type'], 500);
        }

        $userId = $requestData['user_id'];
        $token = $requestData['token'];

        $user = $this->resource->getAuth($userId, $token);
        if (!$user) {
            return $response->withJson(['error' => 'Auth Error', 'message' => 'no data'], 404);
        }

        /**
         * @var  \Psr\Http\Message\ResponseInterface $response PSR7 response
         */
        $response = $next($request, $response);

        return $response;
    }
}