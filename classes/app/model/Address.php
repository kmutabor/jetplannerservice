<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 23.08.17
 * Time: 22:41
 */

namespace app\model;

/**
 * @Entity
 * @Table(name="address")
 */
class Address
{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @Column(type="integer") **/
    private $user_id;
    /** @Column(type="string") **/
    private $address;
    /** @Column(name="created_at", type="datetime") **/
    private $created_at;
    /** @Column(name="updated_at", type="datetime") **/
    private $updated_at;


    /**
     * Many Addresses have One User.
     * @ManyToOne(targetEntity="User", inversedBy="addresses")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;



    public function setUser(User $user)
    {
        $user->assignedToAddresses($this);
        $this->user = $user;
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     *
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     *
     */
    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");
    }




    public function toArray() {
        return array(
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'address' => $this->getAddress(),
//            'createdAt' => $this->getCreatedAt(),
//            'updatedAt' => $this->getUpdatedAt(),
        );
    }


}