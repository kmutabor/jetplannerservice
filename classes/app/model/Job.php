<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 23.08.17
 * Time: 22:41
 */

namespace app\model;

/**
 * @Entity
 * @Table(name="job")
 */
class Job
{

    /**
     * Many Devices have One User.
     * @ManyToOne(targetEntity="User", inversedBy="jobs")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function setUser(User $user)
    {
        $user->assignedToJob($this);
        $this->user = $user;
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @Column(type="integer") **/
    private $user_id;
    /** @Column(type="integer") **/
    private $address_id;
    /** @Column(type="string") **/
    private $name;
    /** @Column(type="string") **/
    private $description;
    /** @Column(name="is_deleted", type="boolean") **/
    private $is_deleted;
    /** @Column(name="created_at", type="datetime") **/
    private $created_at;
    /** @Column(name="updated_at", type="datetime") **/
    private $updated_at;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * @param mixed $address_id
     */
    public function setAddressId($address_id)
    {
        $this->address_id = $address_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param mixed $is_deleted
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }


    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");
    }


    public function toArray() {
        return array(
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'address_id' => $this->getAddressId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'is_deleted' => $this->isDeleted(),
//            'created_at' => $this->getCreatedAt(),
//            'updated_at' => $this->getUpdatedAt(),
        );
    }


}