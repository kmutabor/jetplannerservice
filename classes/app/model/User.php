<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="user")
 */
class User
{


    /**
     * Many Users have Many Addresses.
     * @OneToMany(targetEntity="Address", mappedBy="user")
     */
    protected $addresses;

    /**
     * One User has Many Jobs.
     * @OneToMany(targetEntity="Job", mappedBy="user")
     */
    protected $jobs;

    /**
     * One User has Many Events.
     * @OneToMany(targetEntity="Event", mappedBy="user")
     */
    protected $events;

    /**
     * One User has Many Devices.
     * @OneToMany(targetEntity="Device", mappedBy="user")
     */
    protected $devices;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->events= new ArrayCollection();
        $this->devices= new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @Column(type="string") **/
    private $login;
    /** @Column(type="string") **/
    private $password;
    /** @Column(type="string") **/
    private $token;
    /** @Column(name="is_deleted", type="integer") **/
    private $is_deleted;
    /** @Column(type="string") **/
    private $phone;
    /** @Column(name="created_at", type="datetime") **/
    private $created_at;
    /** @Column(name="updated_at", type="datetime") **/
    private $updated_at;

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @param bool $isDeleted
     */
    public function setIsDeleted($is_deleted)
    {
        $this->is_deleted = $is_deleted;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");
    }


    public function assignedToEvent(Event $event)
    {
        $this->events[] = $event;
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }



    public function assignedToJob(Job $job)
    {
        $this->jobs[] = $job;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }



    public function assignedToDevice(Device $device)
    {
        $this->devices[] = $device;
    }

    /**
     * @return ArrayCollection
     */
    public function getDevices()
    {
        return $this->devices;
    }

    public function assignedToAddresses(Address $address)
    {
        $this->addresses[] = $address;
    }

    /**
     * @return ArrayCollection
     */
    public function getAddresses() {
        return $this->addresses;
    }




    public function checkIfExistDeviceByImei($imei){
        if($this->getDevices()){
            /**
             * @var Device $device
             */
            foreach ($this->getDevices() as $device){
                if($device->getImei() === $imei){
                    return true;
                }
            }
        }
        return false;
    }

    public function toArray(){
        $addresses = [];
        foreach ($this->getAddresses() as $address){
            /**
             * @var Address $address
             */
            $addresses[]=$address->toArray();
        }

        $jobs = [];
        foreach ($this->getJobs() as $job){
            /**
             * @var Job $job
             */
            $jobs[]=$job->toArray();
        }

        $events = [];
        foreach ($this->getEvents() as $event){
            /**
             * @var Event $event
             */
            $events[]=$event->toArray();
        }

        $devices = [];
        foreach ($this->getDevices() as $device){
            /**
             * @var Device $device
             */
            $devices[]=$device->toArray();
        }
        return array(
            'id' => $this->getId(),
            'phone' => $this->getPhone(),
            'login' => $this->getLogin(),
            'password' => $this->getPassword(),
            'token' => $this->getToken(),
            'is_deleted' => $this->isDeleted(),
//            'created_at' => $this->getCreatedAt(),
//            'updated_at' => $this->getUpdatedAt(),
            'devices' => $devices,
            'jobs' => $jobs,
            'events' => $events,
            'addresses' => $addresses,
        );

    }



}