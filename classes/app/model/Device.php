<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 23.08.17
 * Time: 22:41
 */

namespace app\model;

/**
 * @Entity
 * @Table(name="devices")
 */
class Device
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @Column(type="integer") **/
    private $user_id;
    /** @Column(type="string", length=50) **/
    private $imei;
    /** @Column(type="string") **/
    private $name;

    /**
     * Many Devices have One User.
     * @ManyToOne(targetEntity="User", inversedBy="devices")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function setUser(User $user)
    {
        $user->assignedToDevice($this);
        $this->user = $user;
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * @param mixed $imei
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /** @Column(type="string") **/


    public function toArray(){
        return array(
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'name' => $this->getName(),
            'imei' => $this->getImei(),
        );

    }

}