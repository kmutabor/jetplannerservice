<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 23.08.17
 * Time: 22:41
 */

namespace app\model;

/**
 * @Entity
 * @Table(name="events")
 */
class Event
{

    /**
     * Many Events have One User.
     * @ManyToOne(targetEntity="User", inversedBy="events")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function setUser(User $user)
    {
        $user->assignedToEvent($this);
        $this->user = $user;
    }
    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** @Column(type="integer") **/
    private $user_id;
    /** @Column(type="integer") **/
    private $job_id;
    /** @Column(type="integer") **/
    private $status_id;
    /** @Column(type="datetime") **/
    private $desired_time;
    /** @Column(name="created_at", type="datetime") **/
    private $created_at;
    /** @Column(name="updated_at", type="datetime") **/
    private $updated_at;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getJobId()
    {
        return $this->job_id;
    }

    /**
     * @param mixed $job_id
     */
    public function setJobId($job_id)
    {
        $this->job_id = $job_id;
    }

    /**
     * @return mixed
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * @param mixed $status_id
     */
    public function setStatusId($status_id)
    {
        $this->status_id = $status_id;
    }

    /**
     * @return mixed
     */
    public function getDesiredTime()
    {
        return $this->desired_time;
    }

    /**
     * @param mixed $desired_time
     */
    public function setDesiredTime($desired_time)
    {
        $this->desired_time = $desired_time;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");;
    }



    public function toArray() {
        return array(
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'job_id' => $this->getJobId(),
            'status_id' => $this->getStatusId(),
            'desired_time' => $this->getDesiredTime(),
//            'createdAt' => $this->getCreatedAt(),
//            'updatedAt' => $this->getUpdatedAt(),
        );
    }


}