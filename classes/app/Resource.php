<?php
/**
 * Created by PhpStorm.
 * User: mutabor
 * Date: 23.08.17
 * Time: 20:40
 */

namespace app;

use app\model\Address;
use app\model\Device;
use app\model\Job;
use app\model\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Noodlehaus\Config;

class Resource
{
    /**
     * @var Config|null $conf
     */
    private $conf = null;


    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;
    private static $instance = null;

    public function __construct()
    {
        $this->conf = new Config( './../config/config.yml');
        $this->entityManager = $this->getEntityManager();
    }


    public static function getInstance(){
        if (self::$instance === null) {
            self::$instance = new Resource();
        }

        return self::$instance;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->createEntityManager();
        }

        return $this->entityManager;
    }

    /**
     * @return EntityManager
     */
    private function createEntityManager()
    {
        $path = array('app/model');
        $devMode = true;

        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode);

        $connectionOptions = array(
            'driver'   => $this->conf->get('db.driver'),
            'host'     => $this->conf->get('db.host'),
            'dbname'   => $this->conf->get('db.dbname'),
            'user'     => $this->conf->get('db.user'),
            'password' => $this->conf->get('db.password'),
        );

        return EntityManager::create($connectionOptions, $config);
    }

    public function getAuth($userId = null, $token = null){
        $query = $this->entityManager->createQuery('SELECT u FROM app\model\User u WHERE u.id = ?1 AND u.token = ?2');
        $query->setParameter(1, $userId);
        $query->setParameter(2, $token);
        $userData = $query->getOneOrNullResult();

        return $userData;
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function getUser($id)
    {
            return $this->entityManager->find('app\model\User', $id);
    }

    /**
     * @return string
     */
    public function getUsers()
    {
            $users = $this->entityManager->getRepository('app\model\User')->findAll();
            $users = array_map(function($user) {
                return $user->toArray(); },
                $users);
            return $users;
    }


    /**
     * @param User $user
     * Добавить пользователя
     */
    public function addUser(User &$user){
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param Device $device
     * добавить Устройство пользователя
     */
    public function addDevice(Device &$device){
        $this->entityManager->persist($device);
        $this->entityManager->flush();
    }

    /**
     * @param Address $address
     * добавить Адрес пользователя
     */
    public function addAddress(Address &$address){
        $this->entityManager->persist($address);
        $this->entityManager->flush();
    }

    /**
     * @param Job $job
     */
    public function addJob(Job &$job){
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    /**
     * @param $user_id
     * @return array
     */
    public function getAddressesByOwnerId($user_id){
        $addresses = $this->entityManager->getRepository('app\model\Address')->findBy(['user_id'=>$user_id]);
        return $addresses;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getAddresses(array $ids){
        $addresses = $this->entityManager->getRepository('app\model\Address')->findBy(['id'=>$ids]);
        return $addresses;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getAddressById($id){
        $address = $this->entityManager->getRepository('app\model\Address')->find($id);
        return $address;
    }


    public function getUserByPhone($phone){
        $query = $this->entityManager->createQuery(
            'SELECT u FROM app\model\User u WHERE u.phone = ?3 ');
        $query->setParameter(3, $phone);
        /**
         * @var User $user
         */
        $user = $query->getOneOrNullResult();
        return $user;
    }

    public function getUserByLogin($login){
        $query = $this->entityManager->createQuery(
            'SELECT u, d FROM app\model\User u JOIN u.devices d WHERE u.login = ?2 ');
        $query->setParameter(2, $login);
        /**
         * @var User $user
         */
        $user = $query->getOneOrNullResult();
        return $user?$user->toArray():null;
    }

    /**
     * Получить пользоателя по номеру телефона, логину и imei устройства
     *
     * @param $login
     * @param $phone
     * @return array|null
     */
    public function getUserByLoginAndPhoneAndImei($login, $phone, $imei){
        $query = $this->entityManager->createQuery(
            'SELECT u, d FROM app\model\User u LEFT JOIN u.devices d WHERE u.login = ?1 OR u.phone = ?2 OR d.imei = ?3');
        $query->setParameter(1, $login);
        $query->setParameter(2, $phone);
        $query->setParameter(3, $imei);
        /**
         * @var User $user
         */
        return $query->getOneOrNullResult();
    }


    /**
     * Получить jobs по
     * @param $id
     * @return string
     */
    public function getJobsByUserId($id){
        $jobs = $this->entityManager->getRepository('app\model\Job')->findBy(['user_id'=>$id]);
        return $jobs;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getJobsByIds(array $ids){
        $addresses = $this->entityManager->getRepository('app\model\Job')->findBy(['id'=>$ids]);
        return $addresses;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getJobsById($id){
        $address = $this->entityManager->getRepository('app\model\Job')->find($id);
        return $address;
    }


    /**
     * @param $id
     * @return mixed
     */
    public function deleteUser($id){
        $query = $this->entityManager->createQuery('DELETE FROM app\model\User u WHERE u.id = ?1');
        $query->setParameter(1, $id);
        return $query->execute();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteAddress($id){
        $query = $this->entityManager->createQuery('DELETE FROM app\model\Address a WHERE a.id = ?1');
        $query->setParameter(1, $id);
        return $query->execute();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteJob($id){
        $query = $this->entityManager->createQuery('DELETE FROM app\model\Job j WHERE j.id = ?1 ');
        $query->setParameter(1, $id);
        return $query->execute();
    }

    /**
     * @param $id
     * @param $userId
     * @return mixed
     */
    public function deleteJobByIdAndUserId($id, $userId){
        $query = $this->entityManager->createQuery('DELETE FROM app\model\Job j WHERE j.id = ?1 AND j.user_id = ?2');
        $query->setParameter(1, $id);
        $query->setParameter(2, $userId);
        return $query->execute();
    }



}