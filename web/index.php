<?php

use Noodlehaus\Config;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$conf = new Config('./../config/config.yml');
// Start PHP session
session_start();
/**
 * debug
 */
$config['displayErrorDetails'] = $conf->get('displayErrorDetails');
$config['addContentLengthHeader'] = $conf->get('addContentLengthHeader');

/**
 * init app
 */
$app = new \Slim\App(['settings'=>$config]);

$app->get('/', function ($request, $response) {
    return $response->getBody()->write(json_encode(['error'=>'error']));
});


$app->group('/api', function () use ($app) {

    $app->get('/address', function (Request $request, Response $response){
        $requestData = $request->getQueryParams();
        $address_id = $requestData['address_id'];
        $address_owner_id = $requestData['address_owner_id'];

        $resource = \app\Resource::getInstance();

        $returnData = [];
        $addresses = null;
        if($address_owner_id){
            // адреса по id владельца
            $addresses = $resource->getAddressesByOwnerId($address_owner_id);
        }else if(is_array($address_id)){
            // адреса по списку Id адвесов
            $addresses = $resource->getAddresses($address_id);
        }else{
            // адрес по id
            $addresses = $resource->getAddressById($address_id);
        }
        if(isset($addresses) && is_array($addresses)){
            /**
             * @var \app\model\Address $address
             */
            foreach ($addresses as $address){
                $returnData[] = [
                    'id'=>$address->getId(),
                    'user_id'=>$address->getUserId(),
                    'address'=>$address->getAddress(),
                ];
            }
        }else if(isset($addresses)){
            $returnData[] = [
                'id'=>$addresses->getId(),
                'user_id'=>$addresses->getUserId(),
                'address'=>$addresses->getAddress(),

            ];
        }

        return $response->withJson($returnData, 200);
    });

    $app->post('/address', function (Request $request, Response $response){
        $requestData = $request->getParsedBody();
        $userId = $requestData['user_id'];
        $address = $requestData['address'];

        $resource = \app\Resource::getInstance();

        $addressModel = new \app\model\Address();
        $addressModel->setAddress($address);
        $addressModel->setCreatedAt();
        $addressModel->setUpdatedAt();

        $userModel =$resource->getUser($userId);
        $addressModel->setUser($userModel);

        $returnData = [];
        try{

            $resource->getEntityManager()->beginTransaction();
            $resource->addAddress($addressModel);
            $resource->getEntityManager()->commit();

            $returnData['id'] = $addressModel->getId();
            $returnData['user_id'] = $userId;
            $returnData['address'] = $addressModel->getAddress();

        }catch (Exception $e){
            $resource->getEntityManager()->rollback();
            $returnData['id'] = 0;
            $returnData['user_id'] = $userId;
            $returnData['address'] = "";
            return $response->withJson($returnData, 200);
        }

        return $response->withJson($returnData, 200);
    });


    $app->delete('/address/{address_id}/{user_id}/{token}', function (Request $request, Response $response, $args) {
        $addressId = $args['address_id'];
        $userId = $args['user_id'];

        $returnData = [];
        $resource = \app\Resource::getInstance();
        $addressData = $resource->deleteAddress($addressId);
        if ($addressData) {
            $returnData['id'] = $addressId;
            $returnData['user_id'] = $userId;
            $returnData['address'] = "";
        } else {
            $returnData['id'] = $addressId;
            $returnData['user_id'] = $userId;
            $returnData['address'] = "";
        }
        return $response->withJson($returnData, 200);

    });


    $app->get('/user', function (Request $request, Response $response){
        $requestData = $request->getQueryParams();
        $userId = $requestData['user_id'];
        $resource = \app\Resource::getInstance();
        $user = $resource->getUser($userId);

        return $response->withJson($user->toArray(), 200);
    });

    $app->delete('/user/{user_id}/{token}', function (Request $request, Response $response, $args) {
        $userId = $args['user_id'];
        $returnData = [];
        $resource = \app\Resource::getInstance();
        $jobData = $resource->deleteUser($userId);
        if ($jobData) {
            $returnData['id'] = $userId;
            $returnData['phone'] = "";
            $returnData['login'] = "";
            $returnData['token'] = "";
            $returnData['password'] = "";
            $returnData['is_deleted'] = 1;
            $returnData['error'] = "";
            $returnData['code'] = 0;
            $returnData['message'] = "Success";
        } else {
            $returnData['id'] = $userId;
            $returnData['phone'] = "";
            $returnData['login'] = "";
            $returnData['token'] = "";
            $returnData['password'] = "";
            $returnData['is_deleted'] = 0;
            $returnData['error'] = "Delete error";
            $returnData['code'] = 1;
            $returnData['message'] = "Failure";
        }
        return $response->withJson($returnData, 200);

    });


    $app->get('/job', function (Request $request, Response $response){
        $requestData = $request->getQueryParams();
        $job_id = $requestData['job_id'];
        $job_owner_id = $requestData['job_owner_id'];

        $resource = \app\Resource::getInstance();

        $returnData = [];
        $jobs = null;
        if($job_owner_id){
            $jobs = $resource->getJobsByUserId($job_owner_id);
        }else if(isset($job_id) && is_array($job_id)){
            $jobs = $resource->getJobsByIds($job_id);
        }else if(isset($job_id)){
            $jobs = $resource->getJobsById($job_id);
        }
        if (is_array($jobs)){
            /**
             * @var \app\model\Job $job
             */
            foreach ($jobs as $job){
                $returnData[] = [
                    'id'=>$job->getId(),
                    'user_id'=>$job->getUserId(),
                    'address_id'=>$job->getAddressId(),
                    'name'=>$job->getName(),
                    'description'=>$job->getDescription(),
                ];
            }
        }else if ($jobs !== null){
            /**
             * @var \app\model\Job $jobs
             */
            $returnData[] = [
                'id'=>$jobs->getId(),
                'user_id'=>$jobs->getUserId(),
                'address_id'=>$jobs->getAddressId(),
                'name'=>$jobs->getName(),
                'description'=>$jobs->getDescription(),
            ];

        }

        return $response->withJson($returnData, 200);
    });

    $app->post('/job', function (Request $request, Response $response){
        $requestData = $request->getParsedBody();
        $userId = $requestData['user_id'];
        $jobName = $requestData['job_name'];
        $jobDescription = $requestData['job_description'];

        $jobModel = new \app\model\Job();
        $jobModel->setUserId($userId);
        $jobModel->setName($jobName);
        $jobModel->setDescription($jobDescription);
        $jobModel->setIsDeleted(false);
        $jobModel->setCreatedAt();
        $jobModel->setUpdatedAt();

        $resource = \app\Resource::getInstance();

        $userModel =$resource->getUser($userId);
        $jobModel->setUser($userModel);

        $returnData = [];
        try{
            $resource->getEntityManager()->beginTransaction();
            $resource->addJob($jobModel);
            $resource->getEntityManager()->commit();

            $returnData['id'] = $jobModel->getId();
            $returnData['user_id'] = $jobModel->getUserId();
            $returnData['address_id'] = $jobModel->getAddressId();
            $returnData['name'] = $jobModel->getName();
            $returnData['description'] = $jobModel->getDescription();

        }catch (Exception $e){
            $resource->getEntityManager()->rollback();
            $returnData['id'] = 0;
            $returnData['user_id'] = $userId;
            $returnData['address_id'] = 0;
            $returnData['name'] = "";
            $returnData['description'] = "";
            return $response->withJson($returnData, 200);
        }

        return $response->withJson($returnData, 200);
    });


    $app->delete('/job/{job_id}/{user_id}/{token}', function (Request $request, Response $response, $args) {
        $jobId = $args['job_id'];
        $userId = $args['user_id'];

        $returnData = [];

        $resource = \app\Resource::getInstance();
        $jobData = $resource->deleteJobByIdAndUserId($jobId, $userId);


        $returnData['id'] = 0;
        $returnData['user_id'] = $userId;
        $returnData['address_id'] = 0;
        $returnData['name'] = "";
        $returnData['description'] = "";

        return $response->withJson($returnData, 200);
    });




})->add(new Auth());


/**
 * запрос регистрации
 * todo: надо сделать, что бы не было возможности спама
 */
$app->post('/api/registration', function (Request $request, Response $response){
    $requestData = $request->getParsedBody();

    $password = $requestData['password'];
    $phone = $requestData['phone'];

    if(!$password || !$phone){
        return $response->withJson(['code'=>1, 'error'=>'Invalid request error','message'=>'Check what api request arguments you past.'], 500);
    }

    $resource = \app\Resource::getInstance();
    /**
     * @var \app\model\User $user
     */
    $user = $resource->getUserByPhone($phone);

    $returnData = [];
    if(!$user){
        $newUser = new \app\model\User();
        $newUser->setCreatedAt();
        $newUser->setUpdatedAt();
        $newUser->setLogin($phone);
        $newUser->setPassword($password);
        $newUser->setPhone($phone);
        $newUser->setToken(md5($phone.$password.$phone.time()));
        $newUser->setIsDeleted(false);

        try{
            // begin transaction
            $resource->getEntityManager()->beginTransaction();

            // save new user
            $resource->addUser($newUser);

            $device = new \app\model\Device();
            $device->setName($phone.'_'.$phone);
            $device->setImei($phone);
            $device->setUser($newUser);

            // save device for new user
            $resource->addDevice($device);

            // end transaction
            $resource->getEntityManager()->commit();

            $returnData['token']=$newUser->getToken();
            $returnData['user_id']=$newUser->getId();
            $returnData['message']="Success";
            $returnData['code']=0;
            $returnData['error']=0;
        }catch (Exception $e){
            $resource->getEntityManager()->rollback();
            $returnData['token']="";
            $returnData['user_id']=0;
            $returnData['message']="database Error";
            $returnData['code']=1;
            $returnData['error']="Failure";
        }
    }else{
        $returnData['token']="";
        $returnData['user_id']=0;
        $returnData['message']="This phone number already registered";
        $returnData['code']=1;
        $returnData['error']="Already Exist";
    }
    return $response->withJson($returnData, 200);
});








$app->run();

